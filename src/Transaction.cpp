#include "Transaction.h"
// #include "key.h"
#include "sha256.h"
#include <iostream>
#include <vector>
#include <sstream>


Transaction::Transaction(const std::vector<unsigned char>& PubKeyReceiver,
			 const std::vector<unsigned char>& SignatureSender,
			 double Shares) {
  this->time = time_t(nullptr);
  this->PubKeyReceiver = PubKeyReceiver;
  this->SignatureSender = SignatureSender;
  this->Shares = Shares;
  this->hash = this->_CalculateHash();
}

// bool Transaction::Verify(uint256 hash, const std::vector<unsigned char>& Signature, const CKey Owner) {
//   return Owner::Verify(Owner.GetPubKey(), hash, Signature);
// }

std::string Transaction::_CalculateHash() const {
  std::stringstream ss;
  std::string PublicKeyReceiverS = std::string(
				      reinterpret_cast<const char*>(&PubKeyReceiver[0]),
				      PubKeyReceiver.size());

  std::string SignatureSenderS = std::string(
				      reinterpret_cast<const char*>(&SignatureSender[0]),
				      SignatureSender.size());
  ss << PublicKeyReceiverS << SignatureSenderS << Shares << time;
  std::string hashS = sha256(ss.str());

  return hashS;
}

bool CheckCredit() {
  return true; // Comprobamos que gaste dinero propio
}
