#ifndef TRANSACTION_H
#define TRANSACTION_H

//  #include "key.h"
#include <time.h>
#include <vector>
#include <string>

class Transaction {
public:
  Transaction(const std::vector<unsigned char>& PubKeyReceiver, const std::vector<unsigned char>& SignatureSender, double Shares);
  // ADD constructor
  
  //  bool Verify(uint256 hash, const std::vector<unsigned char>& Signature, const CKey Owner);

  bool CheckCredit(); // Comprobamos que no gaste dinero que no tiene

  std::string _CalculateHash() const;
  
private:
  time_t time;
  std::string hash;
  std::vector<unsigned char> PubKeyReceiver;
  std::vector<unsigned char> SignatureSender;
  double Shares;
};

#endif  // TRANSACTION_H
